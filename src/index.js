#!/bin/env /usr/bin/node
const { Command } = require('commander')
const { getVer } = require('./utils/version')
const { Delete } = require('./utils/delete')
const { up, upImports } = require('./utils/up')
const { update } = require('./utils/update')
const { Install } = require('./utils/install')
const { loadJSON, saveJSON } = require('./lib/json')
const fs = require('fs')
const { AutoComplete } = require('enquirer');
const { Dl, sleep } = require('./lib')
const { addRepo } = require('./utils/addrepo')
const { checkIfArrayIsUnique } = require('./lib/checks')


function getInfo(p) {
    let desc = `=======${p.name}=======\ndescription: ${p.description}\nversion: ${p.version}\narch: ${p.arch.join(', ')}\ndownload link: ${p.dl}\n`
    return desc
}

let cli = new Command()

cli
    .name("nova")
    .version(getVer().toString())

cli.command("install <pkg...>")
    .alias("add")
    .option("-a, --arch [arch]", "arch", "x86_64")
    .option("-r, --repo [[repo].db]", "repo")
    .action(function (pkg) {
        // console.log(pkg)
        Install(pkg, this.opts().arch, this.opts().repo)
    })

cli.command("update")
    .description("updates / syncs that package database")
    .action(async function () {
        await update()
    })


cli.command("uninstall <pkg>")
    .alias("remove")
    .description("removes package")
    .action(function (pkg) {
        Delete(pkg)
    })

cli.command("up")
    .description("sets up all directorys")
    .action(async function () {
        up()
    })

cli.command("search [pkg]")
    .alias('find')
    .description("find a package")
    .action(async (pkg) => {
        let repo = []
        let tmp
        let repoFiles = fs.readdirSync('/etc/nova/db.d/')
        repoFiles.forEach(async (file) => {
            let repoData
            let npkg
            try {
                repoData = JSON.parse(fs.readFileSync('/etc/nova/db.d/' + file))
                // let hasDuplicate = repoData.packages.some((val, i) => arr.indexOf(val) !== i);
                if (repoData.packages === undefined) {
                    return
                }
                // npkg = repoData.packages
                // console.log(npkg)              
            } catch (error) {
                // throw error
                process.stdout.write(`[${file}] has invalid json. couldn't load\n`)
                return
            }
            repo = [
                ...repo,
                ...repoData.packages
            ]
        })
        if (pkg !== undefined) {
            try {
                let p = repo.find(x => x.name === pkg)
                process.stdout.write(getInfo(p))
                return
            } catch (error) {
                process.stdout.write(`package ${pkg} is not in this repo\n`)
                return
            }
        }
        sleep(500)
        // let repo = JSON.parse(fs.readFileSync('/etc/nova/db.d/core.db', { encoding: 'utf-8', flag: 'r' }))  
        let prompt = new AutoComplete({
            name: 'pkg',
            message: 'enter the package you are looking for => ',
            choices: [
                ...new Set(repo)
            ],
        })
        await prompt.run()
            .then((a) => {
                let p = repo.find(x => x.name === a)
                process.stdout.write(getInfo(p))
            })
    })

cli.command("addrepo")
    .description("add a package repo")
    .action(() => {
        addRepo()
    })

cli.parse()

module.exports = { cli }