const https = require('https');
const fs = require('fs');
const { ProgressBar } = require('./progress')
const { exec } = require('child_process');


const Bar = new ProgressBar();


const sleep = ms => new Promise(resolve => setTimeout(resolve, ms))
async function Dl(url, name, dir, bar = true) {
    https.get(url, (res) => {
        const path = dir + name;

        if (bar) {
            const filePath = fs.createWriteStream(path);
            const total = res.headers["content-length"];
            let current = 0;

            Bar.init(total);

            res.on("data", chunk => {
                current += chunk.length;
                Bar.update(current);
            });
            res.pipe(filePath);
            filePath.on('finish', () => {
                filePath.close();
            })
            res.on("error", e => process.stdout.write(e.message));
        }
        else {
            const filePath = fs.createWriteStream(path);
            res.pipe(filePath);
            filePath.on('finish', () => {
                filePath.close();
            })
        }
    })
}
async function Rm(filePath) {
    fs.access(filePath, error => {
        if (!error) {
            fs.unlink(filePath, function (error) {
                if (error) {
                    process.stdout.write(error+'\n');
                }
            });
        } else {
            process.stdout.write(error+'\n');
        }
    });
}

async function Run(command) {
    return exec(command, (error, stdout, stderr) => {
        process.stdout.write(stdout+'\n')
        process.stdout.write(stderr+'\n')
        if (error !== null) {
            process.stdout.write(`exec error: ${error}\n`);
        }
    });
}

module.exports = { Dl, Rm, sleep, Run }