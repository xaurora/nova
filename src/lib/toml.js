const fs = require('fs')
const toml = require('@iarna/toml')

async function loadTOML(filename = '') {
    return toml.parse(fs.readFileSync(filename))
}

async function saveTOML(filename, data) {
    return fs.writeFileSync(filename, toml.stringify(data))
}

module.exports = { saveTOML, loadTOML }