const { prompt } = require('enquirer');
const json = require('../lib/json')

async function addRepo() {
    if (process.getuid() == 0) {
        const response = await prompt([
            {
                type: 'input',
                name: 'repoName',
                message: 'What is the name of the repo?'
            },
            {
                type: 'input',
                name: 'repoUrl',
                message: 'What is your repo url?'
            }
        ]);
        if (response.repoName && response.repoUrl) {
            let data = {
                "repo": {
                    "url": response.repoUrl
                }   
            }
            
            json.saveJSON(`/etc/nova/db.d/${response.repoName}.db`, data)
            process.stdout.write("please run 'nova update'\n")
        } else {
            process.stdout.clearScreenDown()
            process.stdout.write('you can leave fields empty\n')
        }
    }
}

module.exports = { addRepo }