const { Rm } = require("../lib")
const { existsSync } = require('fs')
let Delete = (pkg) => {
    if (process.getuid() == 0) {
        if (existsSync(`./testt/${pkg}.xapp`)) {
            Rm(`./testt/${pkg}.xapp`)
        } else {
            process.stdout.write(`"${pkg}" is not installed\n`)
        }
    } else {
        process.stdout.write('must run as root\n')
    }
}

module.exports = { Delete }