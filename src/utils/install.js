const { readFileSync, existsSync, mkdirSync, unlinkSync, readdirSync } = require('fs');
const { Dl, Run, sleep } = require('../lib');
const fs = require('node:fs')
const unzip = require('unzipper');
const { unlink, rmdir, rm } = require('fs/promises');
const { up } = require('./up');
const { loadTOML } = require('../lib/toml');


let dbf = '/etc/nova/db.d/'
let tmpf = "/tmp/novadb/"

async function Install(package, arch, file = 'core.db') {
    if (!existsSync('/etc/nova/db.d/', { recursive: true })) {
        mkdirSync('/etc/nova/db.d/', { recursive: true })
    }
    if (existsSync(dbf + file)) {
        if (process.geteuid() == 0) {
            package.forEach(async (package) => {
                if (!existsSync('/tmp/nova/apps/')) {
                    mkdirSync('/tmp/nova/apps/', { recursive: true })
                }
                if (!existsSync('/tmp/nova/apps/files/')) {
                    mkdirSync('/tmp/nova/apps/files/', { recursive: true })
                }
                let json = readFileSync(dbf + file)
                let j = JSON.parse(json)
                if (!j.packages) {
                    process.stdout.write(`[${file}] invalid repo try updating this repo\n`)
                    return;
                }
                let t = j.packages.find(x => x.name === package)?.dl
                if (!j.packages.find(x => x.name === package)?.dl) {
                    process.stdout.write(`[${file}] package ${package} not found. skipping\n`)
                    return
                };
                let url = t
                if (t) {
                    try {
                        process.stdout.write('downloading package\n')
                        await Dl(url, `${package}.xapp`, '/tmp/nova/apps/files/')
                        await sleep(1000)
                        fs.createReadStream(`/tmp/nova/apps/files/${package}.xapp`).pipe(unzip.Extract({ path: `/tmp/nova/apps/` }));
                        await sleep(1000)
                        process.stdout.write('building package\n')
                        process.chdir(`/tmp/nova/apps/${package}/`)
                        let xpkg = await loadTOML('XPKG')
                        await Run(`sudo bash ${xpkg.scripts.build}`)
                        await sleep(1000)
                        process.stdout.write('cleaning up\n')
                        await unlink(`/tmp/nova/apps/files/${package}.xapp`)
                        await rm(`/tmp/nova/apps/${package}/`, { recursive: true, force: true })
                        process.stdout.write("DONE!\n")
                    } catch (error) {
                        throw error
                    }
                }
            });
        } else {
            process.stdout.write('must run as root\n')
        }
    } else {
        up()
    }
}

module.exports = { Install }