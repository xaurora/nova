const { existsSync, mkdirSync, readFileSync } = require('fs')
const { Dl } = require('../lib/index')
async function up() {
    if (process.getuid() == 0) {
        if (!existsSync('/etc/nova')) {
            mkdirSync('/etc/nova')
        }
        if (!existsSync('/etc/nova/conf')) {
            mkdirSync('/etc/nova/conf')
        }
        if (!existsSync('/etc/nova/db.d')) {
            mkdirSync('/etc/nova/db.d')
        }
        if (!existsSync('/etc/nova.db.d/core.db')) {
            Dl('https://gitlab.com/xaurora/nova-core-repo/-/raw/main/repo.json?inline=false', 'core.db', '/etc/nova/db.d/')
        }
    } else {
        process.stdout.write('must run as root\n')
    }
}

async function upImports() {
    return JSON.parse(readFileSync('/etc/nova/db.d/core.db', { encoding: 'utf-8', flag: 'r' }))
}

module.exports = { up, upImports }