const { readFileSync, readFile, readdir, existsSync, mkdirSync, unlinkSync, unlink } = require('fs');
const { randomBytes } = require('crypto')
const path = require('path');
const fs = require('fs');
const { Dl, sleep } = require('../lib/index')


let dbf = '/etc/nova/db.d/'
let tmpf = "/tmp/nova/db/"

function check1(name) {
    return readFileSync(tmpf + name)
}


async function checkDBs(folder) {
    try {
        await readdir(folder, async (err, files) => {
            files.forEach(async (file) => {
                if (process.getuid() == 0) {
                    process.stdout.write(`started downloading ${file}\n`)
                    let json = readFileSync(dbf + file)
                    let j = JSON.parse(json)
                    let name = path.parse(file).name;
                    let XtmpBuffer
                    let tmpBuffer
                    let f = `${file}.${randomBytes(5).toString('hex')}.json`
                    if (!j.repo) {
                        process.stdout.write(`[${name}] invalid repo object try
"repo": {
    "url": "your repo url"
}\n`)
                        return
                    }
                    await Dl(j.repo.url, f, tmpf, false).catch(error => { throw error })
                    process.stdout.write(`[${file}] done fetching online version\n`)
                    await sleep(1000)
                    XtmpBuffer = check1(f)
                    tmpBuffer = XtmpBuffer.compare(json)
                    if (tmpBuffer) {
                        fs.copyFile(tmpf + f, dbf + file, (err) => {
                            if (err && err.code == "EACCES") {
                                process.stdout.write("you have to be root use this command\n")
                            } else if (err) {
                                process.stdout.write(err)
                            }
                            process.stdout.write(`[${name}]: Update finished\n`)
                        });
                    } else {
                        process.stdout.write(`[${name}]: Up to date\n`)
                    }
                } else {
                    process.stdout.write('This program must be run as root\n')
                }

            });
        });
    } catch (error) {
        throw error
    }

}

let update = async () => {
    if (process.getuid() == 0) {
        let t1 = existsSync(tmpf)
        let t2 = existsSync(dbf)
        if (!t1) {
            mkdirSync(tmpf, { recursive: true })
        }
        if (!t2) {
            mkdirSync(dbf, { recursive: true })
        }
        await checkDBs(dbf)
    } else {
        process.stdout.write('must run as root\n')
    }
}


module.exports = { update }