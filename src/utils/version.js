let getVer = () => {
    try {
        const p = require('../../package.json')
        return p.version.toString()
    } catch (error) {
        process.stdout.write(error)
    }
}

module.exports = { getVer } 